# Just Story
  **To read**: http://rs.delivery.epam.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fjust-story
   
  **Estimated reading time**: 10 mins
   
  ## Story Outline
  Just story to show functionality of Refactoring Stories tool.
   
  ## Story Organization
  **Story Branch**: master
  > `git checkout master`
   
  
  Tags: story
